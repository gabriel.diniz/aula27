public class aula27 {  // laço de repetição

    public static void main(String[] args) {

        int i = 0; // variavel i valor 0

        do {  // instrução faça
            System.out.println( "do/while" );  // imprimi expressao
        }while (i != 0); // i diferente de 0? imprimi uma vez, do while sempre imprimir uma vez


        while (i != 0){  // i diferente de 0? verificou condição , não é verdadeira, não executou
            System.out.println("while"); // não imprimi
        }
        }
}

